var debug = {

    on: true,
    mobile: false,

    log: function() {
        if(this.on){
            for(var i = 0; i < arguments.length; i++){
                if(this.mobile) $('#error-log').prepend(arguments[i] + '<br>');
                else console.warn('DEBUG MSG: ', arguments[i]);
            }
        }
    },
    error: function(){
        if(this.on){

            for(var i = 0; i < arguments.length; i++){
                if(this.mobile) $('#error-log').prepend('ERROR: ' + arguments[i] + '<br>');
                else console.error('DEBUG ERR:', arguments[i]);
            }
        }
    },
    clear: function(){
        if(this.on){
            if(document.cookie)document.cookie = '';

            if(localStorage && typeof localStorage != 'undefined'){
                for (var key in localStorage){
                    if(localStorage.hasOwnProperty(key))delete localStorage[key];
                }
            }
            location.reload();
        }
    }
};


$(function(){

    if(debug.mobile){
        $('body').append(
            '<div id="error-log"><input type="text" id="error-log-input"><div id="debug-cache" title="Очистка кэша внутри интерактива убъет конфиг и кнопку ВЕРНУТЬСЯ">Очистить память</div></div>' +
            '<style>#error-log { position: fixed; background: rgba(255,255,255,1); left: 0; right: 0; ' +
            'top: -140px; height: 160px; border: 1px solid #000; font: 20px Arial; color: black; padding: 5px 5px 21px; ' +
            'box-sizing: border-box; z-index: 999999; overflow: auto; transition: top ease .5s; }#error-log:hover, #error-log:focus {top: 0;}  ' +
            '#error-log-input { display: block; position: absolute; left: 0; right: 0; bottom: 0; width: 100%; box-sizing: border-box; ' +
            'font-size: 20px; }  #debug-cache { position: absolute; right: 0; top: 0; padding: 5px; background: red; cursor: pointer; } </style>');

        $('#error-log-input').on('keyup', function(e){
            if(e.keyCode == 13){
                var code = $(this).val();
                eval(code);
                $(this).val('');
            }
        });

        $('#debug-cache').click(function(){
            debug.clear();
        });
    }

    window.onerror = function(error, url, line) {
        $('#error-log').prepend(error + ' - ' + url + ' - ' + line + '<br>');
    };

});