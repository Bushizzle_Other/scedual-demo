if(typeof debug == 'undefined')debug = false;

Object.prototype.size = function() {return Object.keys(this).length};

var scedualChanges = {};

$(function(){

    // dictionary

    var weekDays = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        months = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

    // data

    var data, dl;

    // useful global vars

    var oneDay = 1000 * 60 * 60 * 24,
        now = new Date(),
        firstYear = parseInt(now.getFullYear()) - (now.getMonth() < 7);

    var start = new Date(firstYear, 8, 1),
        diff = now - start,
        todayDay = Math.floor(diff / oneDay);

    // calendar period vars

    var currentFrom = todayDay, // current day num
        currentTo = currentFrom + 6,
        currentPeriod,
        isBegin = false;

    if(debug)debug.log(currentFrom, currentTo, oneDay);

    // stats vars

    var todayMinutes = 0,
        todayMinutesPassed = 0,
        periodMinutes = 0,
        periodMinutesPassed = 0;

    // ui

    var $getAll = $('.diary__header-all'),
        $getCurrent = $('.diary__header-current'),
        $wrap = $('.diary__week'),
        $current = $('.header__dates-arrow-current'),
        $goNext = $('.header__dates-arrow_next'),
        $goPrev = $('.header__dates-arrow_prev'),
        $getPeriod = $('.diary__header-show-period'),
        $showPeriod = $('.diary__header-showperiod'),
        $periodPop = $('.diary__header-period'),
        $periodError = $('.diary__header-perioderror');

    // stats

    var $periodAllLessons = $('#periodAllLessons'),
        $periodAllHours = $('#periodAllHours'),
        $periodDoneLessons = $('#periodDoneLessons'),
        $periodDoneHours = $('#periodDoneHours'),
        $periodLeftLessons = $('#periodLeftLessons'),
        $periodLeftHours = $('#periodLeftHours');

    var $todayAllLessons = $('#todayAllLessons'),
        $todayAllHours = $('#todayAllHours'),
        $todayDoneLessons = $('#todayDoneLessons'),
        $todayDoneHours = $('#todayDoneHours'),
        $todayLeftLessons = $('#todayLeftLessons'),
        $todayLeftHours = $('#todayLeftHours');

    // core

    function setPeriodStatistics(){
        $periodAllLessons.text( periodMinutes/40 + ' уроков' );
        $periodAllHours.text( ( (periodMinutes/60 >= 1) ? (Math.floor(periodMinutes/60) + ' часов ') : '') + ( (periodMinutes%60 > 0) ? (periodMinutes%60 + ' минут') : '') );
        $periodDoneLessons.text( periodMinutesPassed/40 + ' уроков' );
        $periodDoneHours.text( ( (periodMinutesPassed/60 >= 1) ? (Math.floor(periodMinutesPassed/60) + ' часов ') : '' ) + ( (periodMinutesPassed%60 > 0) ? (periodMinutesPassed%60 + ' минут') : '') );
        $periodLeftLessons.text( (periodMinutes-periodMinutesPassed)/40 + ' уроков' );
        $periodLeftHours.text( ( ((periodMinutes-periodMinutesPassed)/60 >= 1) ? (Math.floor((periodMinutes-periodMinutesPassed)/60) + ' часов ') : '') + ( ((periodMinutes-periodMinutesPassed)%60 > 0) ? ((periodMinutes-periodMinutesPassed)%60 + ' минут') : '') );
    }

    function setTodayStatistics(){
        $todayAllLessons.text( todayMinutes/40 + ' уроков' );
        $todayAllHours.text( ( (todayMinutes/60 >= 1) ? (Math.floor(todayMinutes/60) + ' часов ') : '' ) + ( (todayMinutes%60 > 0) ? (todayMinutes%60 + ' минут') : '') );

        $todayDoneLessons.text( todayMinutesPassed/40 + ' уроков' );
        $todayDoneHours.text( ( (todayMinutesPassed/60 >= 1) ? (Math.floor(todayMinutesPassed/60) + ' часов ') : '' ) + ( (todayMinutesPassed%60 > 0) ? (todayMinutesPassed%60 + ' минут') : '') );

        $todayLeftLessons.text( (todayMinutes-todayMinutesPassed)/40 + ' уроков' );
        $todayLeftHours.text( ( ((todayMinutes-todayMinutesPassed)/60 >= 1) ? (Math.floor((todayMinutes-todayMinutesPassed)/60) + ' часов ') : '' ) + ( ((todayMinutes-todayMinutesPassed)%60 > 0) ? ((todayMinutes-todayMinutesPassed)%60 + ' минут') : '') );
    }

    function getScedual(user){
        $.ajax({
            url: 'test.json' // get test data
        }).done(function (response) {
            data = response.data;
            dl = data.length - 1;

            $('.diary__header-name').text(response.username);

            if(debug)debug.log('current day num: ' + currentFrom);

            drawScedual(true);
            setTodayStatistics();
        });
    }

    function drawScedual(isInit){

        periodMinutes = 0;
        periodMinutesPassed = 0;

        $wrap.empty();

        var targetMilliseconds = (start - 0) + currentFrom*oneDay,
            target = new Date(targetMilliseconds),
            dayCounter = target.getDay();


        if(isInit){
            var dayOffset = 0;
            if(dayCounter == 0) dayOffset = -1;
            if(dayCounter > 1) dayOffset = dayCounter - 1;

            dayCounter = 1;

            currentFrom-=dayOffset;
            currentTo-=dayOffset;

            if(debug)debug.log('Start init from ' + weekDays[dayCounter]);
        }

        if(currentFrom < 0) {
            dayCounter-=currentFrom;
            currentFrom = 0;
            $goPrev.hide();
            isBegin = true;
        } else {
            $goPrev.show();
        }

        if(currentFrom > dl){
            currentFrom = dl;
        }

        if(currentTo > dl) {
            currentTo = dl;
            $goNext.hide();
        } else {
            $goNext.show();
        }

        var dateFrom = new Date(start - 0 + currentFrom*oneDay),
            dateTo = new Date(start - 0 + currentTo*oneDay),
            resDate = (currentFrom == currentTo) ? dateFrom.getDate() + ' ' + months[dateFrom.getMonth()] : dateFrom.getDate() + ' ' + months[dateFrom.getMonth()] + ' - ' + dateTo.getDate() + ' ' + months[dateTo.getMonth()],
            yearFrom = dateFrom.getFullYear(),
            yearTo = dateTo.getFullYear(),
            resYear = (yearFrom == yearTo) ? yearFrom : yearFrom + ' - ' + yearTo;


        $current.html(resDate + ', <b>' + resYear + '</b>');

        for(var a = currentFrom; a < currentTo + 1; a++){

            var todayMS = start - 0 + a*oneDay,
                today = new Date(todayMS);


            if(data.hasOwnProperty(a)){

                var isSunday = (dayCounter == 0) ? ' diary__day_sunday' : '';
                var isToday = (todayDay == a) ? ' diary__day_today' : '';

                var $template = $('<div/>')
                    .addClass('diary__day' + isSunday + isToday)
                    .attr('data-day', a)
                    .append(
                    $('<div/>')
                        .addClass('diary__day-name')
                        .append($('<span/>').text(weekDays[dayCounter] + ', ' + today.getDate() + ' ' + months[today.getMonth()])))
                    .append($('<div/>')
                        .addClass('diary__day-content')
                        .append(function(){

                            var lessons = '';

                            if(data[a].size() > 0){

                                for(var b in data[a]){
                                    if(data[a].hasOwnProperty(b)){

                                        var discipline = data[a][b].discipline || '<i>Окно</i>',
                                            classIndex = data[a][b].classIndex || '',
                                            isDone = (typeof data[a][b].isDone == 'undefined') ? '' : '<div class="isDone__box"><input class="isDone__checkbox" data-day="' + a + '" data-pos="' + b + '" type="checkbox" ' + ((data[a][b].isDone) ? 'checked' : '') + ' /><div class="isDone__bg"></div></div>',
                                            time = 10 + (b - 0);

                                        if(data[a][b].discipline) {
                                            periodMinutes+=40;

                                            if(isToday != ''){
                                                todayMinutes+=40;
                                            }
                                        }

                                        if(data[a][b].isDone) {
                                            periodMinutesPassed+=40;

                                            if(isToday != ''){
                                                todayMinutesPassed+=40;
                                            }
                                        }

                                        if(isDone != '' && scedualChanges.hasOwnProperty(a) && scedualChanges[a].hasOwnProperty(b)){

                                            isDone = '<div class="isDone__box"><input class="isDone__checkbox" data-day="' + a + '" data-pos="' + b + '" type="checkbox" ' + ((scedualChanges[a][b]) ? 'checked' : '') + ' /><div class="isDone__bg"></div></div>';

                                            if(scedualChanges[a][b]) {
                                                periodMinutesPassed+=40;

                                                if(isToday != ''){
                                                    todayMinutesPassed+=40;
                                                }
                                            }
                                        }

                                        lessons += '<div class="diary__lesson' + ( (data[a][b].isDone) ? ' diary__lesson_passed' : '' ) + '" data-position="' + b + '">' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_number">' + (b - 0 + 1) + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_time">' + time + ':00'  + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_classIndex"><a href="#">' + classIndex + '</a></div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_discipline">' + discipline + '</div>' +
                                            '<div class="diary__lesson-cell diary__lesson-cell_isDone">' + isDone + '</div>' +
                                            '</div>';

                                    }

                                }

                            } else {

                                lessons = $('<div/>', { class: "diary__weekend" } );

                            }

                            return lessons;

                        }));

                $wrap.append($template);

                if(dayCounter < 6) dayCounter++;
                else {
                    dayCounter = 0;
                }

            }

        }

        $('.isDone__checkbox').change(function(){ // TODO jQuery .on()

            var $el = $(this),
                dayNum = $el.data('day'),
                lessonNum = $el.data('pos'),
                thisPassedModify = ($el.prop('checked')) ? 1 : -1;

            periodMinutesPassed+=40*thisPassedModify;

            if(dayNum == todayDay) todayMinutesPassed+=40*thisPassedModify;

            setPeriodStatistics();

            if(scedualChanges.hasOwnProperty(dayNum) && scedualChanges[dayNum].hasOwnProperty(lessonNum)){

                delete scedualChanges[dayNum][lessonNum];
                if(scedualChanges[dayNum].size() == 0) delete scedualChanges[dayNum];

            } else {

                if(!scedualChanges.hasOwnProperty(dayNum)) scedualChanges[dayNum] = {};

                scedualChanges[dayNum][lessonNum] = $(this).prop('checked');

            }

            $el.closest('.diary__lesson').toggleClass('diary__lesson_passed');

            if(debug)debug.log(scedualChanges);
        });

        $('body, html').scrollTop(0);

        setPeriodStatistics();
    }

    function skipPeriod(x){

        if(data){

            currentPeriod = currentTo - currentFrom + 1;

            currentFrom = currentFrom + x*currentPeriod;
            currentTo = currentTo + x*currentPeriod;

            if(isBegin) {
                currentTo++; // костыль
                isBegin = false;
            }

            drawScedual();

        }
    }

    function periodError(txt){
        $periodError.text(txt).show();
        setTimeout(function(){
            $periodError.text('').hide();
        }, 1000);
    }

    // events

    $getAll.click(function(){
        currentFrom = 0;
        currentTo = data.length - 1;

        drawScedual();

        $getAll.add($goNext).add($goPrev).hide();
        $getCurrent.show();
    });

    $getCurrent.click(function(){
        currentFrom = Math.floor(diff / oneDay);
        currentTo = currentFrom + 6;

        drawScedual(true);

        $getCurrent.hide();
        $getAll.add($goNext).add($goPrev).show();
    });

    $goNext.click(function(){skipPeriod(1)});
    $goPrev.click(function(){skipPeriod(-1)});

    $getPeriod.click(function(){
        $periodPop.toggle();
    });

    $showPeriod.click(function(){

        var sf = Math.floor((Date.parse($('#scFrom').val()) - start)/oneDay),
            st = Math.floor((Date.parse($('#scTo').val()) - start)/oneDay);

        if(sf >= 0 && sf < dl && st >= 0 && st < dl){

            if(st > sf){

                currentFrom = sf;
                currentTo = st;

                drawScedual();

                $goNext.add($goPrev).add($getAll).add($periodPop).hide();
                $getCurrent.show();

            } else {

                periodError('Ошибка: неверный порядок');
                if(debug)debug.error(sf, st, dl);

            }

        } else {

            if(st < 0 || st > dl || sf < 0 || sf > dl){

                periodError('Ошибка: даты вне учебного года');

            } else {

                periodError('Ошибка: не указаны даты');

            }

            if(debug)debug.error(sf, st, dl);
        }

    });

    // Statistics

    $('.diary__header-statistics').click(function(){

        $('.diary__header-info').toggleClass('active');

    });

    $(document).ready(getScedual);

    // Debug

    if(debug){
        $('.diary__header-title').click(function(){
            debug.log('from ' + currentFrom, 'to ' + currentTo, 'data length: ' + data.length);
        });
    }

});
